# taller3

Formas Glut y transformaciones

El proyecto contiene 2 archivos .rar.

-El archivo FormasGlut.rar, que dibuja figuras predefinidas de opengl.
![ejecucion FormasGlut](https://www.dropbox.com/s/4pcx71g71rjc4g9/formasGlut.PNG?dl=0)

-El archivo Transformaciones_Basicas.rar, que contiene el programa en netbeans que realiza transformaciones como trasladar, rotar y escalar un triangulo
![ejecucion Transformaciones figura](https://www.dropbox.com/s/tf03lua5j4gma92/transformaciones%20figura.PNG?dl=0)